import {Row, Col, Button, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function errorPage(){
	return (
		<Row>
			<Col className = "p-5">
			<h1>404 - Not Found</h1>
				<p>The page you are looking for cannot be found</p>
				<Button variant="primary" as={Link} to="/" >Back Home</Button>
			</Col>
		</Row>
	)
} 


// import Banner from '../components/Banner';
// export default function errorPage(){

/*
	12 - connect backend
	end  of session
	
	15 - Mock Technical Exam (2 exams)
	- 5 function coding
	- 2 - flashback 
	* more creation of function

	16-18 Capstone 3 Development
	** 6 Sessions 

	19 - Holiday

	22 - Present or Finish Capstone
	-- hosting
	1:00- 3:30 Commencement

	*/