import {Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Login(){

	//Allows us to consume the User Context object and its properties to use for user validation
	// useContext(Provider);
	const {user, setUser} = useContext(UserContext);
	console.log(user);

	//State Hooks to store the values of the input fields
	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');

	const [isActive, setIsActive]= useState(false);

	function authenticate(e) {

		e.preventDefault();

		//Set the email of the authenticated user in the local storage
		/*Syntax
			localStorage.setItem('propertyName/key',value);
		*/
		localStorage.setItem("email",email);

		// Access user information through localStorage to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
		//When states change components are rerendered and the AppNavBar component will be updated based on the credentials.
		setUser({
			email: localStorage.getItem('email')
		});

		setEmail('');
		setPassword('');

		alert('Successfully Login');
	};


	useEffect(() => {
		if (email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email,password]);
	
	return(
	(user.email !== null ) ?
		<Navigate to ="/courses"/>
	:	
	<>
	<h1>Log In</h1>
	<Form onSubmit ={e => authenticate(e)}>
		<Form.Group controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value = {email}
				onChange = {e => setEmail(e.target.value)}
			/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>	
		</Form.Group>

		<Form.Group controlId="password1">
			<Form.Label>Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value = {password}
				onChange = {e => setPassword(e.target.value)}
			/>
		</Form.Group>
		{ isActive ? 
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Log In
			</Button>

			:

			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Log In
			</Button>

		}
	</Form>
	</>
	)
}